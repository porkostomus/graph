(ns graph.core
  (require 'rhizome.viz))

(def tree1
  [a [b [c] [d] [e]] [f [g] [h]]])

(def t 
[1 [2 [3 [4] [5]] [6 [7] [8]]] [9 [10 [11] [12]] [13 [14 [15]]]]]])

(defn tree [t f]
  (save-tree next rest t
    :filename f
    :node->descriptor (fn [n] {:label (first n)})))

(tree '(n) "tree.png")

(tree '(:A (:B (:D) (:E)) (:C (:F))) "tree2.png")

(tree '(a (b (c) (d) (e)) (f (g) (h))) "tree4a.png")
(tree '(d (b (c) (e) (a (f (g) (h))))) "tree4b.png")
(tree '(a (b (c (d) (e)) (f (g) (h))) (i (j (k) (l)) (m (n) (o)))) "tree5a.png")
(tree '(c (d) (e) (b (f (g) (h)) (a (i (j (k) (l)) (m (n) (o)))))) "tree5b.png")
